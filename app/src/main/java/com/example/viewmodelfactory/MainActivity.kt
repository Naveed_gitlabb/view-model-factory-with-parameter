package com.example.viewmodelfactory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

    class MainActivity : AppCompatActivity() {

        lateinit var txtCountr : TextView
        lateinit var mainViewModel: MainViewModel

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            mainViewModel = ViewModelProvider(this , MianViewModelFactory(10 ) ).get(MainViewModel :: class.java)
            txtCountr = findViewById(R.id.txtContr)
            setText()
        }

        fun increment(v : View){
            mainViewModel.increment()
            setText()
        }

        private fun setText() {
            txtCountr.text = mainViewModel.count.toString()
        }


}